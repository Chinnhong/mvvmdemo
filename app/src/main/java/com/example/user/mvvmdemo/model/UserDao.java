package com.example.user.mvvmdemo.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;
@Dao
public interface UserDao {
    @Insert
    void insert(User user);
    @Update
    void update(User user);
    @Delete
    void delete(User user);
    @Query("DELETE FROM user_table")
    void deleteAllUsers();
    @Query("SELECT * FROM user_table ORDER BY id  DESC")
    LiveData<List<User>> getAllUsers();
}
