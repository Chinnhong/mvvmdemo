package com.example.user.mvvmdemo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.mvvmdemo.model.User;
import com.example.user.mvvmdemo.viewmodel.UserViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btnInsert,btnDelete,btnUpdate;
    EditText etUsername, etEmail;
    RecyclerView recyclerView;
    private UserViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsername = findViewById(R.id.etUsername);
        etEmail = findViewById(R.id.etEmail);
        btnDelete = findViewById(R.id.btnDelete);
        btnInsert = findViewById(R.id.btnInsert);
        btnUpdate = findViewById(R.id.btnUpdate);

        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        final UserAdapter adapter = new UserAdapter();
        recyclerView.setAdapter(adapter);
        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        viewModel.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                adapter.setUsers(users);
                etEmail.setText("");
                etUsername.setText("");
            }
        });
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUsername.getText().toString().equals("")){
                    etUsername.setError("Require");
                    return;
                }
                else if(etEmail.getText().toString().equals("")){
                    etEmail.setError("Require");
                    return;
                }

                viewModel.insert(new User(etUsername.getText().toString(),etEmail.getText().toString()));
                Toast.makeText(MainActivity.this, "Inserted", Toast.LENGTH_SHORT).show();
            }
        });

        adapter.setOnItemClickListener(new UserAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final User user) {
                etUsername.setText(user.getUserName());
                etEmail.setText(user.getUserEmail());
                btnDelete.setEnabled(true);
                btnUpdate.setEnabled(true);
                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(etUsername.getText().toString().equals("")){
                            etUsername.setError("Require");
                            return;
                        }
                        else if(etEmail.getText().toString().equals("")){
                            etEmail.setError("Require");
                            return;
                        }
                        User userUpdate = new User(etUsername.getText().toString(),etEmail.getText().toString());
                        userUpdate.setId(user.getId());
                        viewModel.update(userUpdate);
                        btnDelete.setEnabled(false);
                        btnUpdate.setEnabled(false);
                        Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                    }
                });
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewModel.delete(user);
                        btnDelete.setEnabled(false);
                        btnUpdate.setEnabled(false);
                        Toast.makeText(MainActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

}
