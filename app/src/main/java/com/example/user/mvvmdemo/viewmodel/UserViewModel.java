package com.example.user.mvvmdemo.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.user.mvvmdemo.UserRepo;
import com.example.user.mvvmdemo.model.User;

import java.util.List;

public class UserViewModel extends AndroidViewModel {
    private UserRepo userRepo;
    private LiveData<List<User>> allUsers;
    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepo = new UserRepo(application);
        allUsers= userRepo.getAllUsers();
    }
    public void insert(User user){
        userRepo.insert(user);
    }
    public void update (User user){
        userRepo.update(user);
    }
    public void delete(User user){
        userRepo.delete(user);
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }
}
